package ru.t1.amsmirnov.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerUpdateSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerUpdateSchemeResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class ServerUpdateSchemeCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "drop-scheme";

    @NotNull
    public static final String DESCRIPTION = "Update server database.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE SERVER DATABASE]");
        @NotNull final ServerUpdateSchemeRequest request = new ServerUpdateSchemeRequest(getToken());
        @NotNull final ServerUpdateSchemeResponse response = getServiceLocator().getSystemEndpoint().updateScheme(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
