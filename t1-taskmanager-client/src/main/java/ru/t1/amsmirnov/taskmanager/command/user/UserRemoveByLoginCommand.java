package ru.t1.amsmirnov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserRemoveRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserRemoveResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-remove-by-login";

    @NotNull
    public static final String DESCRIPTION = "Remove user by login.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), login);
        @NotNull final UserRemoveResponse response = getUserEndpoint().removeByLogin(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
