package ru.t1.amsmirnov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserRemoveByEmailRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserRemoveByEmailResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class UserRemoveByEmailCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-remove-by-email";

    @NotNull
    public static final String DESCRIPTION = "Remove user by email.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRemoveByEmailRequest request = new UserRemoveByEmailRequest(getToken(), email);
        @NotNull final UserRemoveByEmailResponse response = getUserEndpoint().removeByEmail(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
