package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.service.ITokenService;

public final class TokenService implements ITokenService {

    @Nullable
    private String token;

    @Override
    public @Nullable String getToken() {
        return token;
    }

    @Override
    public void setToken(@Nullable final String token) {
        this.token = token;
    }

}
