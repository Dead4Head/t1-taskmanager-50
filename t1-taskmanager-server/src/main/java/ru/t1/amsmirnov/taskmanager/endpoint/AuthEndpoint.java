package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLogoutRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLoginResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLogoutResponse;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        try {
            @NotNull final String token = getServiceLocator().getAuthService().login(request.getLogin(), request.getPassword());
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            return new UserLoginResponse(e);
        }
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            getServiceLocator().getAuthService().logout(session);
            return new UserLogoutResponse();
        } catch (@NotNull final Exception e) {
            return new UserLogoutResponse(e);
        }
    }

}
