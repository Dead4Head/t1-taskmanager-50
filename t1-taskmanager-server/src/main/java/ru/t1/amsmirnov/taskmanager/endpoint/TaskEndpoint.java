package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.response.task.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final Status status = request.getStatus();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().changeStatusById(userId, id, status);
            return new TaskChangeStatusByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse removeAllTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            getServiceLocator().getTaskService().removeAll(userId);
            return new TaskClearResponse();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskClearResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().changeStatusById(userId, id, Status.COMPLETED);
            return new TaskCompleteByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().create(userId, name, description);
            return new TaskCreateResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCreateResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse findAllTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final TaskSort sort = request.getSort();
            Comparator<TaskDTO> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull final List<TaskDTO> tasks = getServiceLocator().getTaskService().findAll(userId, comparator);
            return new TaskListResponse(tasks);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskListResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeOneTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().removeOneById(userId, id);
            return new TaskRemoveByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse findOneTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().findOneById(userId, id);
            return new TaskShowByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
            return new TaskStartByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().updateById(userId, id, name, description);
            return new TaskUpdateByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
            return new TaskBindToProjectResponse();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskBindToProjectResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
            return new TaskUnbindFromProjectResponse();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUnbindFromProjectResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByProjectIdResponse findAllTasksByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByProjectIdRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @NotNull final List<TaskDTO> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
            return new TaskShowByProjectIdResponse(tasks);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByProjectIdResponse(e);
        }
    }

}
