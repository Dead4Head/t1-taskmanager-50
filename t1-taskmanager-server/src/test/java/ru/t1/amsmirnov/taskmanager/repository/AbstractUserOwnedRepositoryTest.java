package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IUserDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.repository.dto.UserDtoRepository;

import java.util.UUID;

public abstract class AbstractUserOwnedRepositoryTest extends AbstractRepositoryTest {

    @NotNull
    protected static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    static final UserDTO USER_ALFA = new UserDTO();

    @NotNull
    protected static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    static final UserDTO USER_BETA = new UserDTO();

    protected static IUserDtoRepository USER_REPOSITORY;

    @BeforeClass
    public static void initUsers() throws Exception {

        USER_ALFA.setId(USER_ALFA_ID);
        USER_ALFA.setFirstName("First Name");
        USER_ALFA.setPasswordHash("HASH!!!!");
        USER_ALFA.setLastName("Last Name ");
        USER_ALFA.setMiddleName("Middle Name ");
        USER_ALFA.setEmail("useralfa@tm.ru");
        USER_ALFA.setLogin("alfa");
        USER_ALFA.setRole(Role.USUAL);

        USER_BETA.setId(USER_BETA_ID);
        USER_BETA.setFirstName("First Name");
        USER_BETA.setPasswordHash("HASH!!!!");
        USER_BETA.setLastName("Last Name ");
        USER_BETA.setMiddleName("Middle Name ");
        USER_BETA.setEmail("userbeta@tm.ru");
        USER_BETA.setLogin("beta");
        USER_BETA.setRole(Role.USUAL);

        USER_REPOSITORY = new UserDtoRepository(ENTITY_MANAGER);
        try {
            TRANSACTION.begin();
            USER_REPOSITORY.add(USER_ALFA);
            USER_REPOSITORY.add(USER_BETA);
            TRANSACTION.commit();
        } catch (Exception e) {
            TRANSACTION.rollback();
        }
    }

    @AfterClass
    public static void clearUsers() {
        try {
            TRANSACTION.begin();
            USER_REPOSITORY.removeAll();
            TRANSACTION.commit();
        } catch (final Exception e) {
            TRANSACTION.rollback();
        }
    }

}
