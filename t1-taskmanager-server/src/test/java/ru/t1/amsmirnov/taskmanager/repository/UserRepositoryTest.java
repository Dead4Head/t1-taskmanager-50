package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IUserDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.repository.dto.ProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.repository.dto.UserDtoRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class UserRepositoryTest extends AbstractRepositoryTest {

    @NotNull
    private IProjectDtoRepository projectRepository;

    @NotNull
    private IUserDtoRepository userRepository;

    @NotNull
    private final List<UserDTO> users = new ArrayList<>();

    @Before
    public void initRepository() throws Exception {
        projectRepository = new ProjectDtoRepository(ENTITY_MANAGER);
        userRepository = new UserDtoRepository(ENTITY_MANAGER);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final UserDTO user = new UserDTO();
            user.setFirstName("First Name " + i);
            user.setPasswordHash("HASH!!!!");
            user.setLastName("Last Name " + i);
            user.setMiddleName("Middle Name " + i);
            user.setEmail("user" + i + "@tm.ru");
            user.setLogin("USER" + i);
            user.setRole(Role.USUAL);
            try {
                TRANSACTION.begin();
                userRepository.add(user);
                TRANSACTION.commit();
                users.add(user);
            } catch (final Exception e) {
                TRANSACTION.rollback();
                throw e;
            }
        }
    }

    @After
    public void clearRepository() throws Exception {
        try {
            TRANSACTION.begin();
            userRepository.removeAll();
            TRANSACTION.commit();
            users.clear();
        } catch (final Exception e) {
            TRANSACTION.rollback();
            throw e;
        }
    }

    @Test
    public void testFindByEmail() {
        for (final UserDTO user : users) {
            assertEquals(user, userRepository.findOneByEmail(user.getEmail()));
        }
    }

    @Test
    public void testFindByLogin() {
        for (final UserDTO user : users) {
            assertEquals(user, userRepository.findOneByLogin(user.getLogin()));
        }
    }

    @Test
    public void testIsEmailExist() {
        for (final UserDTO user : users) {
            assertTrue(userRepository.isEmailExist(user.getEmail()));
            assertFalse(userRepository.isEmailExist(""));
        }
    }

    @Test
    public void testIsLoginExist() {
        for (final UserDTO user : users) {
            assertTrue(userRepository.isLoginExist(user.getLogin()));
            assertFalse(userRepository.isEmailExist(user.getLogin() + user.getLogin()));
        }
    }

}
