package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.service.dto.ISessionDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.service.dto.SessionDtoService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(DBCategory.class)
public class SessionServiceTest extends AbstractUserOwnedServiceTest {

    @NotNull
    private static final ISessionDtoService SESSION_SERVICE = new SessionDtoService(CONNECTION_SERVICE);

    @NotNull
    private List<SessionDTO> sessions = new ArrayList<>();

    @NotNull
    private final List<SessionDTO> alfaSessions = new ArrayList<>();

    @NotNull
    private final Comparator<SessionDTO> comparator = (o1, o2) -> {
        if (o1 == o2) return 0;
        return o2.getCreated().compareTo(o1.getCreated());
    };

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final SessionDTO session = new SessionDTO();
            if (i <= 5) {
                session.setUserId(USER_ALFA_ID);
                alfaSessions.add(session);
            } else {
                session.setUserId(USER_BETA_ID);
            }
            sessions.add(session);
            SESSION_SERVICE.add(session);
        }
    }

    @After
    public void clearRepository() {
        SESSION_SERVICE.removeAll();
        sessions.clear();
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAdd_ModelNotFoundException_1() throws AbstractException {
        SESSION_SERVICE.add(USER_ALFA_ID, null);
    }

    @Test
    public void testFindAllUser() throws AbstractException {
        assertEquals(alfaSessions, SESSION_SERVICE.findAll(USER_ALFA_ID, null));
        assertEquals(sessions, SESSION_SERVICE.findAll());
        final Comparator<SessionDTO> comparator = null;
        assertEquals(sessions, SESSION_SERVICE.findAll(comparator));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_UserIdEmptyException_1() throws AbstractException {
        SESSION_SERVICE.findAll("", null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_UserIdEmptyException_2() throws AbstractException {
        SESSION_SERVICE.findAll(NULL_STR, null);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindOneById_ModelNotFoundException_1() throws AbstractException {
        SESSION_SERVICE.findOneById(NONE_STR, NONE_STR);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindOneById_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.removeAll();
        SESSION_SERVICE.findOneById(NONE_STR, NONE_STR);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOne_ModelNotFoundException_1() throws AbstractException {
        SESSION_SERVICE.removeOne(USER_ALFA_ID, null);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOne_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.removeOne(USER_ALFA_ID, new SessionDTO());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOneById_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.removeOneById(USER_ALFA_ID, NONE_STR);
    }

    @Test
    public void testAdd() throws AbstractException {
        final SessionDTO newSession = new SessionDTO();
        newSession.setUserId(USER_ALFA_ID);
        SESSION_SERVICE.add(newSession);
        sessions.add(newSession);
        assertEquals(sessions.size(), SESSION_SERVICE.getSize());
        assertTrue(SESSION_SERVICE.existById(newSession.getId()));
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAdd_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.add(null);
    }


    @Test
    public void testAddAll() throws Exception {
        final List<SessionDTO> newSessions = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            SessionDTO session = new SessionDTO();
            session.setUserId(USER_ALFA_ID);
            newSessions.add(session);
            sessions.add(session);
            Thread.sleep(1);
        }
        SESSION_SERVICE.addAll(newSessions);
        assertEquals(sessions.size(), SESSION_SERVICE.getSize());
        assertEquals(sessions, SESSION_SERVICE.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAddAll_ModelNotFoundException() throws AbstractException {
        SESSION_SERVICE.addAll(null);
    }

    @Test
    public void testSet() throws Exception {
        final List<SessionDTO> newSessions = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            SessionDTO session = new SessionDTO();
            session.setUserId(USER_ALFA_ID);
            newSessions.add(session);
            Thread.sleep(1);
        }
        SESSION_SERVICE.set(newSessions);
        sessions.addAll(newSessions);
        assertEquals(newSessions.size(), SESSION_SERVICE.getSize());
        assertEquals(newSessions, SESSION_SERVICE.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testSet_ModelNotFoundException() throws AbstractException {
        SESSION_SERVICE.set(null);
    }

}
