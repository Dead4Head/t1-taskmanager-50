package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataBackupSaveRequest extends AbstractUserRequest {

    public DataBackupSaveRequest() {
    }

    public DataBackupSaveRequest(@Nullable final String token) {
        super(token);
    }

}
