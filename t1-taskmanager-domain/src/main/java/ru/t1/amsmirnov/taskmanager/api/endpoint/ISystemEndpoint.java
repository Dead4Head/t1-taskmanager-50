package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerDropSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerUpdateSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerDropSchemeResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerUpdateSchemeResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull String NAME = "SystemEndpoint";

    @NotNull String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerAboutRequest serverAboutRequest);

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerVersionRequest serverVersionRequest);

    @NotNull
    @WebMethod
    ServerUpdateSchemeResponse updateScheme(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerUpdateSchemeRequest request);

    @NotNull
    @WebMethod
    ServerDropSchemeResponse dropScheme(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerDropSchemeRequest request);

}
