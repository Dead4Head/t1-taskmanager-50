package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getMongoDBHost();

    @NotNull
    Integer getMongoDBPort();

    @NotNull
    String getMongoDBName();

    @NotNull
    String getMongoDBDefCollectionName();

}
